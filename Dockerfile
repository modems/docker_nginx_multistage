
FROM debian:9 AS build

RUN apt update && apt install -y wget gcc make libpcre3 libpcre3-dev zlib1g zlib1g-dev
RUN cd /usr/local/src && wget http://luajit.org/download/LuaJIT-2.0.5.tar.gz && tar zxvf LuaJIT-2.0.5.tar.gz && cd LuaJIT-2.0.5 && make && make install PREFIX=/usr/local/LuaJIT
RUN cd /usr/local/src && wget https://github.com/openresty/lua-nginx-module/archive/refs/tags/v0.10.13.tar.gz && tar zxvf v0.10.13.tar.gz

ENV LUAJIT_LIB=/usr/local/LuaJIT/lib
ENV LUAJIT_INC=/usr/local/LuaJIT/include/luajit-2.0

RUN wget https://nginx.org/download/nginx-1.19.3.tar.gz && tar xzvf nginx-1.19.3.tar.gz && cd nginx-1.19.3 && ./configure \
--with-ld-opt="-Wl,-rpath,/usr/local/LuaJIT/lib"  \  
--add-module=/usr/local/src/lua-nginx-module-0.10.13 \
&& make && make install

FROM debian:9

COPY --from=build /usr/local/LuaJIT/lib /usr/local/LuaJIT/lib
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin .
RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx
CMD ["./nginx", "-g", "daemon off;"]